# README

Projet qemotion :

 - Ruby = 2.5.1
- Ruby on Rails = 5.2.2
- GraphQL (API) -> graphlient

Taches : 

 - Rechercher un utilisateur sur Github -> Réalisé
 - Visualiser la liste des dépôts publics auxquels il contribue -> Réalisé
 - Afficher les statistiques de ses contributions par dépôt -> Non réalisé
 - Pour chaque dépôt auquel il contribue visualiser les langages utilisés -> Réalisé
 - Pour chaque dépôt auquel il contribue sa position dans le top 10 des contributeurs de ce dépôt -> Non réalisé
 - Connaitre le top 3 des langages qu'il utilise le plus fréquemment -> Réalisé
-  Nous souhaitons pouvoir conserver nos recherches pour y revenir ultérieurement -> Réalisé
 - En Bonus : Déploiement du projet rails sur HEROKU ou AWS ou tout autre plateforme en ligne -> Réalisé : https://qemotion-projet-martignon.herokuapp.com

La version sur heroku n'a pas la mise à jour des recherches effectuées à cause d'un bug non identifié.
Mais la verison en local possède cette fonctionnalité.

Je n'ai pas réussi à réaliser les deux fonctionnalités liées à la contribution à un dépôt github car je n'ai pas réussi à identifier le paramètre à récuperer et traiter. 