Rails.application.routes.draw do

  post 'recherche' => 'pages#recherche'

  get 'recherche/erreur' => 'pages#erreur'
  get 'recherche/:login' => 'utilisateurs#show'
  root 'pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
