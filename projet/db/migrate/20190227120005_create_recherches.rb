class CreateRecherches < ActiveRecord::Migration[5.2]
  def change
    create_table :recherches do |t|

      t.timestamps
    end

    add_column :recherches,  :nomRecherche, :string

    add_column :recherches, :utilisateur_id, :integer
    add_index :recherches, :utilisateur_id

  end
end
