class PagesController < ApplicationController

  def home

    #Gestion historique recherche
    if defined? @utilisateur_courant
      #affichage des recherches de l utilisateur
      @recherches = @utilisateur_courant.recherchs.sort {|o1, o2| o2.updated_at<=>o1.updated_at}
    else
      #Creation de l utilisateur le cookie n existe pas
      user = Utilisateur.new
      user.save
      session[:utilisateur_id] = user.id
    end

  end

  def recherche
    if params[:barreRecherche] == ""
      redirect_to("/recherche/erreur")
    else
      redirect_to "/recherche/"+params[:barreRecherche]
    end
  end

  def erreur

  end
end
