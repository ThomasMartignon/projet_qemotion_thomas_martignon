class UtilisateursController < ApplicationController

  #Mise en place de la communication avec l'api
  Client = Graphlient::Client.new('https://api.github.com/graphql',
                                  headers: {
                                      'Authorization' => "Bearer 7c7f9caf6cc11d5907a8659206ab70df92b87b62"
                                  }

  )

  #Permet de compter le nombre de langage plus efficacement
  class Langage
    def initialize(nom, nb)
      @nom = nom
      @nb = nb
    end
    def nom
      @nom
    end
    def nb
      @nb
    end
    def nbb
      @nb+=1
    end
  end

  def show
    #definition de la requete
    query = <<-GRAPHQL
      query ($login: String!){
        user (login: $login) {
          login
          repositoriesContributedTo(first: 100, privacy: PUBLIC){
            nodes{
              name
              languages(first: 100){
                nodes{
                  name
                }
               }
            }
          }
          
        }
      }
    GRAPHQL
    variables = { login: params[:login] }

    #gestion de l exception pour savoir si l utilisateur existe
    begin

    #execution de la requete et recupuration du resultat
    @data = Client.query(query, variables)

    #creation ou mise a jour des recherche
        if @utilisateur_courant.recherchs.any? {|o| o.nomRecherche==params[:login]}
          #Erreur lors de la mise en production, marche que en locale
          recherche = @utilisateur_courant.recherchs.where("nomRecherche==?", params[:login]).first
          recherche.updated_at = DateTime.now
          recherche.save
        else
          recherche = Recherch.new
          recherche.nomRecherche = params[:login]
          recherche.utilisateur_id = @utilisateur_courant.id
          recherche.save
      end

    #gestion du top des langages
      langages = [];

      @data.data.user.repositories_contributed_to.nodes.each do |rep|
        rep.languages.nodes.each do |lang|
          if (!langages.empty?)&&(langages.detect{|o| o.nom==lang.name} != nil)
            langages.detect{|o| o.nom==lang.name}.nbb
          else
            langages.push(Langage.new(lang.name, 1))
          end
        end
      end

      @lang = langages.sort {|o1, o2| o2.nb<=>o1.nb}

      @i = 0

      #redicetion d erreur
    rescue => ex
     redirect_to("/recherche/erreur")
    end


  end
end
