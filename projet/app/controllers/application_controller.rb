class ApplicationController < ActionController::Base
  before_action :set_current_utilisateur

  def set_current_utilisateur
    if session[:utilisateur_id]
      @utilisateur_courant = Utilisateur.find(session[:utilisateur_id])
    end
  end

end
